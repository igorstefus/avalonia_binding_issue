﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BindingProblem.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public double Minimum { get; set; } = 1;

        public double Maximum { get; set; } = 180;

        public double Value { get; set; } = 75;

        public string Description { get; set; } = 
            "In view model minimum is 1, maximum is 180 and value is 75. "+
            "Value property is clamped between maximum and minimum when maximum and minimum change using class handlers for those properties. " +
            "It seems that minimum and maximum change handlers are called before default value from view model for value property is set and since default for double type is 0 it will be clamped to 1. " +
            "Problem is that if value property is update in handlers, initial value (75) from view model is not send to value property. " +
            "As consequence value is 1 and it should be 75.";
    }
}
