﻿using System;
using System.Collections.Generic;
using System.Text;
using Avalonia;
using Avalonia.Controls.Primitives;
using Avalonia.Data;

namespace BindingProblem.Controls
{
    /// <summary>
    /// Control containing test child control.
    /// </summary>
    public class TestControl : TemplatedControl
    {
        /// <summary>
        /// Defines the <see cref="Value" /> property.
        /// </summary>
        public static readonly DirectProperty<TestControl, double> ValueProperty =
            AvaloniaProperty.RegisterDirect<TestControl, double>(
                nameof(Value),
                o => o.Value,
                (o, v) => o.Value = v,
                defaultBindingMode: BindingMode.TwoWay);

        /// <summary>
        /// Defines the <see cref="Maximum" /> property.
        /// </summary>
        public static readonly StyledProperty<double?> MaximumProperty =
            AvaloniaProperty.Register<TestControl, double?>(nameof(Maximum));

        /// <summary>
        /// Defines the <see cref="Minimum" /> property.
        /// </summary>
        public static readonly StyledProperty<double?> MinimumProperty =
            AvaloniaProperty.Register<TestControl, double?>(nameof(Minimum));
        
        private double _value;

        /// <summary>
        /// Input value.
        /// </summary>
        public double Value
        {
            get => _value;
            set => SetAndRaise(ValueProperty, ref _value, value);
        }

        /// <summary>
        /// Gets or sets Maximum.
        /// </summary>
        public double? Maximum
        {
            get => GetValue(MaximumProperty);
            set => SetValue(MaximumProperty, value);
        }

        /// <summary>
        /// Gets or sets Minimum.
        /// </summary>
        public double? Minimum
        {
            get => GetValue(MinimumProperty);
            set => SetValue(MinimumProperty, value);
        }
    }
}
