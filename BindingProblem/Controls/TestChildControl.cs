﻿using System;
using System.Collections.Generic;
using System.Text;
using Avalonia;
using Avalonia.Controls.Primitives;
using Avalonia.Data;

namespace BindingProblem.Controls
{
    /// <summary>
    /// Test control to template bind in parent test control.
    /// </summary>
    public class TestChildControl : TemplatedControl
    {
        /// <summary>
        /// Defines the <see cref="Maximum"/> property.
        /// </summary>
        public static readonly StyledProperty<double?> MaximumProperty =
            AvaloniaProperty.Register<TestChildControl, double?>(nameof(Maximum));

        /// <summary>
        /// Defines the <see cref="Minimum"/> property.
        /// </summary>
        public static readonly StyledProperty<double?> MinimumProperty =
            AvaloniaProperty.Register<TestChildControl, double?>(nameof(Minimum));

        /// <summary>
        /// Defines the <see cref="Value"/> property.
        /// </summary>
        public static readonly DirectProperty<TestChildControl, double> ValueProperty =
            AvaloniaProperty.RegisterDirect<TestChildControl, double>(
                nameof(Value),
                numericControl => numericControl.Value,
                (numericControl, v) => numericControl.Value = v,
                defaultBindingMode: BindingMode.TwoWay);


        private double _value;

        /// <summary>
        /// Gets or sets the maximum allowed value.
        /// </summary>
        public double? Maximum
        {
            get => GetValue(MaximumProperty);
            set => SetValue(MaximumProperty, value);
        }

        /// <summary>
        /// Gets or sets the minimum allowed value.
        /// </summary>
        public double? Minimum
        {
            get => GetValue(MinimumProperty);
            set => SetValue(MinimumProperty, value);
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public double Value
        {
            get => _value;
            set => SetAndRaise(ValueProperty, ref _value, value);
        }

        static TestChildControl()
        {
            MaximumProperty.Changed.AddClassHandler<TestChildControl>(x => x.OnMaximumChanged);
            MinimumProperty.Changed.AddClassHandler<TestChildControl>(x => x.OnMinimumChanged);
        }

        private void OnMinimumChanged(AvaloniaPropertyChangedEventArgs obj)
        {
            SetValue();
        }

        private void OnMaximumChanged(AvaloniaPropertyChangedEventArgs obj)
        {
            SetValue();
        }

        private void SetValue()
        {
            double minimum = Minimum??double.MinValue;
            double maximum = Maximum ?? double.MaxValue;

            Value  = Math.Clamp(Value, minimum, maximum);
        }
    }
}
